import descriptionGenerator
import htmlGenerator

inputCcl = "in.xml"
outputCcl = "out.xml"
outputHtml = "out.html"

descriptionGenerator.Generate(inputCcl, outputCcl)
htmlGenerator.Generate(outputCcl, outputHtml)
