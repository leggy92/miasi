//This file was generated from (Commercial) UPPAAL 4.0.14 (rev. 5615), May 2014

/*
bezpiecze\u0144stwo - Sesja nigdy nie mo\u017ce bez przerwy \u015bci\u0105ga\u0107
*/
E[] not forall(i:int[1,maxClients])clientSession(i).dataTransfer

/*
bezpeiczenstwo nigdy deadlock
*/
A[] not deadlock

/*
bezpieczenstwo - sesja zawsze moze byc w stanie idle 
*/
E[] forall(i:int[1,maxClients])clientSession(i).idle		

/*
bezpiecze\u0144stwo - klient mo\u017ce zawsze by\u0107 w stanie pod\u0142\u0105czania (nigdy nie chce \u015bci\u0105ga\u0107)
*/
E[] forall(i:int[1,maxClients])client(i).connecting

/*
zywotnosc - jak klient\u00a0\u015bci\u0105ga to ma wybran\u0105 sesje
*/
client(1).downloading --> client(1).mySessionId != 0

/*
TEGO NIE UMIESZCZAJ W SPRAWKU
to jest dziwne. Nie powinno dzia\u0142a\u0107. Bo to pokazuje \u017ce je\u017celi client si\u0119 pod\u0142\u0105cza (kt\u00f3ry kolwiek) to idSesji clienta 1 jest 0. Pewnie dzia\u0142a to tak dlatego \u017ce nie szuka ka\u017cdej kombinacji tylko pierwsz\u0105 mo\u017cliw\u0105, a pierwsza mo\u017cliwa jest \u017ce wszyscy klienci s\u0105 connecting wiec i pierwszy zawsze ma ID = 0
*/
forall(i:int[1,maxClients])client(i).connecting --> client(1).mySessionId == 0

/*
zywotnosc- jak klient 1 si\u0119 pod\u0142\u0105cza to nie ma wybranego Id Sesji jak robisz forall po lewej stronie --> to po prawej nie mo\u017cna tego i u\u017cy\u0107 wi\u0119c si\u0119 nie da.
*/
client(1).connecting --> client(1).mySessionId == 0

/*
zywotnosc - jak server czeka na klientow to nie wiaodmo jaka jest nastepna sesja (id == 0)
*/
server.waitingForClients --> nextAvailableSession == 0

/*
\u017cywtono\u015bc je\u017celi wysy\u0142aSynAck to wiadomo jaka jest nast\u0119pna sesja (wi\u0119c r\u00f3\u017cna od 0)
*/
server.sendingSynAck --> nextAvailableSession != 0

/*
\u017cywotno\u015b\u0107 - je\u017celi sesja robi transfer danych to server musi by\u0107 zaj\u0119ty
*/
forall(i:int[1,maxClients])clientSession(i).dataTransfer -->serverBusy == true

/*
Nie wiem dlaczego niedzia\u0142a. Przecie\u017c zawsze si\u0119 da taki ci\u0105g zrobi\u0107 \u017ceby osi\u0105gn\u0105\u0107 dataTransfer. Tymbardziej \u017ce dzia\u0142a warunek z \u017cywotno\u015bci
*/
E<> forall(i:int[1,maxClients])clientSession(i).dataTransfer

/*
osiagalno\u015b\u0107 - server mo\u017ce czekac na klient\u00f3w
*/
E<> server.waitingForClients

/*
osi\u0105galno\u015b\u0107 - ka\u017cdy klient mo\u017ce si\u0119 roz\u0142\u0105cza\u0107
*/
E<> forall(i:int[1,maxClients])client(i).disconnecting

/*
osi\u0105galno\u015b\u0107 - ka\u017cdy klient mo\u017ce \u015bci\u0105ga\u0107
*/
E<> forall(i:int[1,maxClients])client(i).downloading
