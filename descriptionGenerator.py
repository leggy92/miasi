# -*- coding: latin2 -*-

import xml.etree.ElementTree as ElementTree
import sys

class Generate:
    DESCRIPTION_TAG_NAME = 'description'
    def __init__(self, inFile, outFile):
        tree = ElementTree.parse(inFile)
        self.parseXml(tree.getroot())
        #TODO get encoding type from input file.
        #	  also xml version, DOCTYPE and other stuff should be written in output.
        tree.write(outFile, encoding="UTF-8")

    def parseXml(self, treeRoot):
        categoriesFunctionsDict = {
            'person_nam': self.personDescription,
            'road_nam': self.roadDescription,
            'country_nam': self.countryDescription,
            'city_nam': self.cityDescription,
            'organization_nam': self.organizationDescription,
            'media_nam': self.mediaDescription,
            'company_nam': self.companyDescription,
            'institution_nam': self.institutionDescription,
            'event_nam': self.eventDescription,
            'title_nam': self.titleDescription,
            'document_nam': self.documentDescription,
            'web_nam': self.webDescription
            };
        words = []
        htmlCode = ""
        for tok in treeRoot.iter('tok'):
            for ann in tok.findall('ann'):
                if ann.text != '0':
                    words.append(tok)
                    if int(ann.text) == len(words):
                        for word in words:
                            description = ElementTree.SubElement(word, self.DESCRIPTION_TAG_NAME)
                            description.text = categoriesFunctionsDict[ann.attrib['chan']](words)
                            description.tail = "\n   "
                        words = []

    def generateDefaultWikipediaLink(self, tok):
        link = "http://pl.wikipedia.org/wiki/"
        for t in tok:
            link += t.find('lex').find('base').text
            link += "_"
        link = link[:-1]
        alink = "<a href ='" + link + "'>" + link + "</a>"
        return alink

    def generateDefaultMapView(self, tok):
        placeFullName = ""
        for t in tok:
            placeFullName += t.find('lex').find('base').text
            placeFullName += " "
        placeFullName = placeFullName[:-1]
        googleMapsLink = "<iframe width='200' height='150' frameborder='0' style='border:0' \
                src='https://www.google.com/maps/embed/v1/search?q=\
                "+placeFullName+"&key=AIzaSyCdWmVXdMXDCUX5MUJ2amO9e_HfHy26Vzo'></iframe>"
        return(placeFullName+' na mapie: \n'+googleMapsLink)

    def personDescription(self, tok):
        return ("Link do biografii tej osoby w Wikipedii:\n" + str(self.generateDefaultWikipediaLink(tok)))

    def roadDescription(self, tok):
        return (self.generateDefaultMapView(tok))

    def countryDescription(self, tok):
        description = "Link do opisu kraju w Wikipedii:\n"
        description += str(self.generateDefaultWikipediaLink(tok))
        description += "\n"
        description += str(self.generateDefaultMapView(tok))
        return description

    def cityDescription(self, tok):
        description = "Link do opisu miasta w Wikipedii:\n"
        description += str(self.generateDefaultWikipediaLink(tok))
        description += "\n"
        description += str(self.generateDefaultMapView(tok))
        return description

    def organizationDescription(self, tok):
        return ("Link do opisu organizacji w Wikipedii:\n" + str(self.generateDefaultWikipediaLink(tok)))

    def mediaDescription(self, tok):
        return ("Jakis opis");

    def companyDescription(self, tok):
        return ("Jakis opis");

    def institutionDescription(self, tok):
        return ("Jakis opis");

    def eventDescription(self, tok):
        return ("Jakis opis");

    def titleDescription(self, tok):
        return ("Jakis opis");

    def documentDescription(self, tok):
        return ("Jakis opis");

    def webDescription(self, tok):
        return ("Jakis opis");

