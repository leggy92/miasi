# -*- coding: latin2 -*-

import xml.etree.ElementTree as ElementTree
import sys

class Generate:
    DESCRIPTION_TAG_NAME = 'description'
    def __init__(self, inFile, outFile):
        tree = ElementTree.parse(inFile)
        html = self.generateHtml(tree)
        with open (outFile, "w") as outputFile:
            outputFile.write(html)

    def generateHtml(self, treeRoot):
        with open ("template-begin.html", "r") as template:
            html = template.read()

        for tok in treeRoot.iter('tok'):
            description = tok.find(self.DESCRIPTION_TAG_NAME)
            base = tok.find('lex').find('base').text
            word = tok.find('orth').text
            if description != None:
                html += '<a class="clickable" data-toggle="popover" role="button" tabindex data-trigger="focus"\
                        data-placement="bottom" title="{0}" data-content="{1}">{2}</a> \
                        '.format(base,description.text,word)
            else:
                html += word + " "
       
        with open ("template-end.html", "r") as template:
            html += template.read()
        
        return html
